﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVisual
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1.- Localiza el mayor número de una rray que le pases por teclado");
            Console.WriteLine("2.- Lanza un dado de 6 caras tantas veces como sea deseado");
            Console.WriteLine("3.- Ordena un array y lo devuelve");
            Console.WriteLine("4.- Selecciona un objeto aleatorio de un array de cadenas");
            Console.WriteLine("Pulsa cualquier otra tecla para salir");
            int opcion;
            if (int.TryParse(Console.ReadLine(), out opcion))
            {
            }
            int[] array;

            switch (opcion)
            {
                case 1:
                    Console.WriteLine("Introduce la longitud del array del que deseas conocer el mayor número");
                    array = new int[Convert.ToInt32(Console.ReadLine())];
                    for (int i = 0; i < array.Length; i++)
                    {
                        Console.WriteLine("Introduce el número que desees insertar en la posicion " + (i + 1));
                        array[i] = Convert.ToInt32(Console.ReadLine());
                    }

                    Console.WriteLine("El número mayor es " + ClassLibrary1.Class1.numeroMayor(array)) ;
                    break;


                case 2:
                    Console.WriteLine("Introduce la cantidad de veces que te gustaría lanzar el dado");
                    int veces = Convert.ToInt32(Console.ReadLine());
                    ClassLibrary1.Class1.lanzarDado(veces);
                    break;



                case 3:
                    Console.WriteLine("Introduce la longitud del array que deseas ordenar");
                    array = new int[Convert.ToInt32(Console.ReadLine())];
                    for (int i = 0; i < array.Length; i++)
                    {
                        Console.WriteLine("Introduce el número que desees insertar en la posicion " + (i + 1));
                        array[i] = Convert.ToInt32(Console.ReadLine());
                    }
                    ClassLibrary1.Class1.ordenarArray(array);
                    Console.WriteLine("El array ha sido ordenado");
                    break;


                case 4:

                    Console.WriteLine("Introduce la longitud del array del que quieras obtener un objeto aleatorio");
                    String[] arrayC = new String[Convert.ToInt32(Console.ReadLine())];
                    for (int i = 0; i < arrayC.Length; i++)
                    {
                        Console.WriteLine("Introduce el texto que desees insertar en la posicion " + (i + 1));
                        arrayC[i] = Console.ReadLine();
                    }

                    ClassLibrary1.Class1.elegirAleatorio(arrayC);

                    break;


            }


        }


    }
}
