package metodos;

import java.util.Arrays;

public class Metodos {
	
	public static int numeroMayor(int[] array){
		int mayor=Integer.MIN_VALUE;
		for(int i = 0; i<array.length; i++){
			if(array[i]>mayor){
				mayor=array[i];
			}
		}
		
		return mayor;
		
	}
	
	

	public static void lanzarDado(int veces){
		for(int i = 0; i<veces; i++){
			System.out.println((int)(Math.random()*6)+1);
		}
	}
	
	
	public static int[] ordenarArray(int[] array){
		Arrays.sort(array);
		return array;
	}
		
	
	public static String elegirAleatorio(String[] array){
		return array[(int)(Math.random()*(array.length+1))];
		
	}
	
	

}
