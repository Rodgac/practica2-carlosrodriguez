package gui1;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JProgressBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSpinner;

public class ventanalogin extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6779735707052882482L;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventanalogin frame = new ventanalogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventanalogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 707, 708);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.GRAY);
		setJMenuBar(menuBar);
		
		JMenu mnConfiguracion = new JMenu("Configuracion");
		menuBar.add(mnConfiguracion);
		
		JMenuItem mntmConfiguracionDeRed = new JMenuItem("Configuracion de red");
		mnConfiguracion.add(mntmConfiguracionDeRed);
		
		JMenu mnPerfil = new JMenu("Perfil");
		menuBar.add(mnPerfil);
		
		JRadioButtonMenuItem rdbtnmntmConectado = new JRadioButtonMenuItem("Conectado");
		mnPerfil.add(rdbtnmntmConectado);
		
		JRadioButtonMenuItem rdbtnmntmOcupado = new JRadioButtonMenuItem("Ocupado");
		mnPerfil.add(rdbtnmntmOcupado);
		
		JRadioButtonMenuItem rdbtnmntmAusente = new JRadioButtonMenuItem("Ausente");
		mnPerfil.add(rdbtnmntmAusente);
		
		JMenu mnImagenDePerfil = new JMenu("Imagen de perfil");
		mnPerfil.add(mnImagenDePerfil);
		
		JMenuItem mntmSubirImagenDe = new JMenuItem("Subir imagen de perfil");
		mnImagenDePerfil.add(mntmSubirImagenDe);
		
		JMenuItem mntmVerImagenDe = new JMenuItem("Ver imagen de perfil");
		mnImagenDePerfil.add(mntmVerImagenDe);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmBuscarAyuda = new JMenuItem("Buscar ayuda");
		mnAyuda.add(mntmBuscarAyuda);
		
		JMenuItem mntmInformaciom = new JMenuItem("Informaciom");
		mnAyuda.add(mntmInformaciom);
		
		JRadioButtonMenuItem rdbtnmntmPopupsDeAyuda = new JRadioButtonMenuItem("Pop-ups de ayuda");
		mnAyuda.add(rdbtnmntmPopupsDeAyuda);
		
		JMenu menu = new JMenu("");
		menuBar.add(menu);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[40.00][][155.00,grow][][grow][20.00][205.00,grow][grow]", "[41.00][grow][35.00][23.00][45.00][][][154.00,grow][grow][][][][grow][][][][]"));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(153, 0, 0));
		contentPane.add(panel, "cell 2 1 1 5,grow");
		
		JLabel lblNombre = new JLabel("Nombre: \r\n");
		lblNombre.setForeground(Color.WHITE);
		contentPane.add(lblNombre, "cell 4 1,aligny top");
		
		textField = new JTextField();
		contentPane.add(textField, "cell 6 1,growx,aligny top");
		textField.setColumns(10);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a:");
		lblContrasea.setForeground(Color.WHITE);
		contentPane.add(lblContrasea, "cell 4 2,aligny top");
		
		passwordField = new JPasswordField();
		contentPane.add(passwordField, "cell 6 2,growx,aligny top");
		
		JLabel lblNumeroDeCanales = new JLabel("Numero de canales abiertos:\r\n");
		lblNumeroDeCanales.setForeground(Color.WHITE);
		contentPane.add(lblNumeroDeCanales, "cell 4 3");
		
		JSpinner spinner = new JSpinner();
		contentPane.add(spinner, "cell 6 3,growx");
		
		JLabel lblServidor = new JLabel("Servidor:");
		lblServidor.setForeground(Color.WHITE);
		contentPane.add(lblServidor, "cell 4 5");
		
		JComboBox<?> comboBox = new JComboBox<Object>();
		comboBox.setBackground(Color.WHITE);
		contentPane.add(comboBox, "cell 6 5,growx");
		
		JTextArea textArea = new JTextArea();
		contentPane.add(textArea, "cell 2 7 5 1,grow");
		
		JLabel lblIntentosDeConexin = new JLabel("Intentos de conexi\u00F3n");
		lblIntentosDeConexin.setForeground(Color.WHITE);
		contentPane.add(lblIntentosDeConexin, "cell 2 8 3 1");
		
		JSlider slider = new JSlider();
		slider.setBackground(Color.DARK_GRAY);
		slider.setSnapToTicks(true);
		slider.setPaintTicks(true);
		slider.setValue(7);
		slider.setMaximum(15);
		contentPane.add(slider, "cell 6 8,growx");
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Guardar contrase\u00F1a\r\n");
		chckbxNewCheckBox.setBackground(Color.LIGHT_GRAY);
		contentPane.add(chckbxNewCheckBox, "flowx,cell 2 9,growx");
		
		JButton btnIniciarSesin = new JButton("Iniciar Sesi\u00F3n");
		btnIniciarSesin.setBackground(Color.WHITE);
		contentPane.add(btnIniciarSesin, "cell 6 9,alignx center");
		
		JCheckBox chckbxMnatenerSesinIniciada = new JCheckBox("Mantener sesi\u00F3n iniciada");
		chckbxMnatenerSesinIniciada.setBackground(Color.LIGHT_GRAY);
		contentPane.add(chckbxMnatenerSesinIniciada, "cell 2 10,growx");
		
		JButton btnRegistrarse = new JButton("Registrarse\r\n");
		btnRegistrarse.setBackground(Color.WHITE);
		contentPane.add(btnRegistrarse, "cell 6 10,alignx center");
		
		JToggleButton tglbtnModoInvisible = new JToggleButton("Modo invisible");
		contentPane.add(tglbtnModoInvisible, "cell 2 14");
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setForeground(Color.ORANGE);
		progressBar.setValue(76);
		contentPane.add(progressBar, "cell 6 14,growx,aligny center");
	}

}
