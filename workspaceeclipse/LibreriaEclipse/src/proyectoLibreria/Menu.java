package proyectoLibreria;

import java.util.Scanner;

import metodos.Metodos;

public class Menu {

	public static void main(String[] args) {
		System.out.println("1.- Localiza el mayor n�mero de una rray que le pases por teclado");
		System.out.println("2.- Lanza un dado de 6 caras tantas veces como sea deseado");
		System.out.println("3.- Ordena un array y lo devuelve");
		System.out.println("4.- Selecciona un objeto aleatorio de un array de cadenas");
		System.out.println("Pulsa cualquier otra tecla para salir");
		Scanner entrada = new Scanner(System.in);
		int opcion = entrada.nextInt();
		entrada.nextLine();
		
		int[] array;
		switch(opcion){
			case 1 :
			System.out.println("Introduce la longitud del array del que deseas conocer el mayor n�mero");
			array = new int[entrada.nextInt()];
			for(int i = 0; i<array.length; i++){
				System.out.println("Introduce el n�mero que desees insertar en la posicion "+(i+1));
				array[i]=entrada.nextInt();
			}
			
			System.out.println("El n�mero mayor es "+Metodos.numeroMayor(array));
				
			break;	
			
			case 2 :
			System.out.println("Introduce la cantidad de veces que te gustar�a lanzar el dado");
			int veces= entrada.nextInt();
			Metodos.lanzarDado(veces);
			break;
			
			case 3 :
			System.out.println("Introduce la longitud del array que deseas ordenar");
			array = new int[entrada.nextInt()];
			for(int i = 0; i<array.length; i++){
				System.out.println("Introduce el n�mero que desees insertar en la posicion "+(i+1));
				array[i]=entrada.nextInt();
			}
			Metodos.ordenarArray(array);
			System.out.println("El array ha sido ordenado");
			
			break;
			
			case 4 :
			
			System.out.println("Introduce la longitud del array del que quieras obtener un objeto aleatorio");
			String[] arrayC = new String[entrada.nextInt()]; 
			for(int i = 0; i<arrayC.length; i++){
				System.out.println("Introduce el texto que desees insertar en la posicion "+(i+1));
				arrayC[i]=entrada.nextLine();
			}
			
			Metodos.elegirAleatorio(arrayC);
			
			break;
		}
		
		entrada.close();		
	}

}
